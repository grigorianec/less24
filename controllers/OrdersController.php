<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 24.10.18
 * Time: 12:59
 */

namespace app\controllers;

use yii\web\Controller;
use app\models\Order;


class OrdersController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index',[
            'title' => 'Order list',
            'orders' => Order::find()->all(),
        ]);
    }

    public function actionView($id)
    {
        $order = Order::findOne($id);

        return $this->render('view', [
            'order'=> $order,
            ]
        );
    }

    public function actionAdd()
    {
        if(\Yii::$app->request->isPost) {
           // $data = \Yii::$app->request->post();

            $order = new Order();
            $order->load(['Order' => \Yii::$app->request->post()]);
            $order->save();

            $this->redirect('/orders/view?id=' . $order->id);
        }

        return $this->render('add');
    }
}
