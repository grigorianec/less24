<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 24.10.18
 * Time: 12:59
 */

namespace app\controllers;

use app\models\Page;

use yii\web\Controller;

class PagesController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index',[
            'title' => 'Page list',
            'pages' => Page::find()->all(),
        ]);
    }

    public function actionView($id)
    {
        $page = Page::findOne($id);
        return $this->render('view', [
                'page'=> $page,
            ]
        );
    }

    public function actionAdd()
    {
        if(\Yii::$app->request->isPost) {
            // $data = \Yii::$app->request->post();

            $page = new Page();
            $page->load(['Page' => \Yii::$app->request->post()]);
            $page->save();

            $this->redirect('/pages/view?id=' . $page->id);
        }

        return $this->render('add');
    }

    public function actionUpdate($id)
    {
        $page = Page::findOne($id);

        if(\Yii::$app->request->isPost)  {
            $page->load(\Yii::$app->request->post());
            $page->save();

            return $this->redirect('/pages/view?id=' . $page->id);
        }
        return $this->render('update', [
            'page' => $page
        ]);
    }
//
//    /**
//     * @param $id
//     * @return \yii\web\Response
//     * @throws \Exception
//     * @throws \Throwable
//     * @throws \yii\db\StaleObjectException
//     */
//    public function actionDelete($id)
//    {
//        $data = Page::findOne($id);
//
//        $data->delete();
//
//        return $this->redirect(['index']);
//    }

}