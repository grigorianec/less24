<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 24.10.18
 * Time: 12:59
 */

namespace app\controllers;

use yii\db\StaleObjectException;
use yii\web\Controller;
use app\models\Product;

class ProductsController extends Controller
{
//    public function beforeAction($action)
//    {
//        if (\Yii::$app->user->isGuest) {
//            return $this->redirect('/site/login');
//        }
//    }

    public function actionIndex()
    {
        return $this->render('index', [
            'title' => 'Product list',
            'products' => Product::find()->all(),
        ]);
    }

    public function actionView($id)
    {
        $product = Product::findOne($id);
        return $this->render('view', [
                'product' => $product,
            ]
        );
    }

//    public function actionAdd()
//    {
//        if (\Yii::$app->request->isPost) {
//            // $data = \Yii::$app->request->post();
//
//            $product = new Product();
//            $product->load(['Product' => \Yii::$app->request->post()]);
//            $product->save();
//
//            $this->redirect('/products/view?id=' . $product->id);
//        }
//
//        return $this->render('add');
//    }
//
//    public function actionUpdate($id)
//    {
//        $product = Product::findOne($id);
//
//        if (\Yii::$app->request->isPost) {
//            $product->load(\Yii::$app->request->post());
//            $product->save();
//
//            return $this->redirect('/products/view?id=' . $product->id);
//        }
//        return $this->render('update', [
//            'product' => $product
//        ]);
//    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws StaleObjectException
     * @throws \Exception
     * @throws \Throwable
     */
//    public function actionDelete($id)
//    {
//        $data = Product::findOne($id);
//
//        $data->delete();
//
//        return $this->redirect(['index']);
//    }
}