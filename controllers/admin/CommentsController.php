<?php

namespace app\controllers;

use app\models\Comment;

class CommentsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        if (\Yii::$app->request->isPost) {
            $comment = new Comment();
            $comment->load(\Yii::$app->request->post());
            $comment->save();
            return $this->redirect('/order/view?id=' . $comment->postId);
        }

        return $this->redirect('/order');
    }

}
