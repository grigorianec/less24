<?php

use yii\db\Migration;

/**
 * Class m181024_084941_create_table_products
 */
class m181024_084941_create_table_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'alias' => $this->text(),
            'description' => $this->text(),
        ]);

        $this->insert('products', [
            'title'=>'Coat',
            'alias'=>'clothes',
            'description'=>'Very comfortable thing'
        ]);

        $this->insert('products', [
            'title'=>'Jacket',
            'alias'=>'clothes',
            'description'=>'Very comfortable thing'
        ]);

        $this->insert('products', [
            'title'=>'Gloves',
            'alias'=>'arm',
            'description'=>'Very comfortable thing'
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('posts');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181024_084941_create_table_products cannot be reverted.\n";

        return false;
    }
    */
}
