<?php

use yii\db\Migration;

/**
 * Class m181024_094502_create_table_pages
 */
class m181024_094502_create_table_pages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'alias' => $this->text(),
            'content' => $this->text(),
        ]);

        $this->insert('pages', [
            'title'=>'Page 1',
            'alias'=>'List 1',
            'content'=>'Very long text about something'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181024_094502_create_table_pages cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181024_094502_create_table_pages cannot be reverted.\n";

        return false;
    }
    */
}
