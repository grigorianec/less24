<?php

use yii\db\Migration;

/**
 * Class m181024_094519_create_table_orders
 */
class m181024_094519_create_table_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'customer_name' => $this->string(),
            'email' => $this->text(),
            'phone' => $this->text(),
            'feedback' => $this->text(),
        ]);

        $this->insert('orders', [
            'customer_name'=>'Maksym',
            'email'=>'tishmax@mail.ru',
            'phone'=> '+380934023945',
            'feedback'=>'Smart and punctual person.'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181024_094519_create_table_orders cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181024_094519_create_table_orders cannot be reverted.\n";

        return false;
    }
    */
}
