<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comments`.
 */
class m181108_133448_create_comments_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('comments', [
            'id' => $this->primaryKey(),
            'body' => $this->text(),
            'createdAt' => $this->integer()->unsigned(),
            'postId' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-comments-posts',
            'comments',
            'postId',
            'orders',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-comments-posts');
        $this->dropTable('comments');
    }
}
