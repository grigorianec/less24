<?php

use yii\db\Migration;

/**
 * Class m181109_124635_create_table_user
 */
class m181109_124635_create_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(50),
            'password' => $this->string(200),
            'authKey' => $this->string(50),
            'accessToken' => $this->string(50)

        ]);

        $this->insert('users', [
            'username' => 'admin',
            'password' => \Yii::$app->security->generatePasswordHash('admin'),
            'authKey' => 'test100Key',
            'accessToken' => '100-token'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181109_124635_create_table_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181109_124635_create_table_user cannot be reverted.\n";

        return false;
    }
    */
}
