<?php

use yii\db\Migration;

/**
 * Handles adding orders_userId to table `orders`.
 */
class m181112_143653_add_orders_userId_column_to_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'orders_userId', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders', 'orders_userId');
    }
}
