<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comments".
 *
 * @property int $id
 * @property string $body
 * @property int $createdAt
 * @property int $postId
 *
 * @property Order $post
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['createdAt', 'postId'], 'integer'],
            ['createdAt', 'default', 'value' => function(Comment $comment, $attribute) {
                return time();
            }],
            ['postId', 'exist', 'skipOnError' => true, 'targetClass' => Order::class, 'targetAttribute' => ['postId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'body' => 'Body',
            'createdAt' => 'Created At',
            'postId' => 'Post ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'postId']);
    }
}
