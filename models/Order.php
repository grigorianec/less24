<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property string $customer_name
 * @property string $email
 * @property string $phone
 * @property string $feedback
 * @property int $orders_userId
 *
 * @property Comment[] $comments
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'phone', 'feedback'], 'string'],
            [['orders_userId'], 'integer'],
            [['customer_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_name' => 'Customer Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'feedback' => 'Feedback',
            'orders_userId' => 'Orders User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['postId' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'orders_userId']);
    }
}


