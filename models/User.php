<?php

namespace app\models;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            ['id', 'integer'],
            [['username', 'password', 'authKey', 'accessToken'], 'string'],
            ['username', 'unique'],
            ['orders_userId', 'exist', 'skipOnError' => true, 'targetClass' => Order::class, 'targetAttribute' => ['orders_userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where(['accessToken' => $token])->one();
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return array|\yii\db\ActiveRecord
     */
    public static function findByUsername($username)
    {
        return self::find()->where(['username' => $username])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
//        var_dump($password, $this->password, \Yii::$app->security->generatePasswordHash($password));
//        return $this->password === \Yii::$app->security->generatePasswordHash($password);
        return \Yii::$app->security->validatePassword($password, $this->password);
    }
    
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['orders_userId' => 'id']);
    }
}