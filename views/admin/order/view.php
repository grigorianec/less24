<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

$comment = new \app\models\Comment();
$comment->postId = $model->id;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_name',
            'email:ntext',
            'phone:ntext',
            'feedback:ntext',
        ],
    ]) ?>


    <?php
    $form = ActiveForm::begin([
        'action' => '/comments/create'
    ])
    ?>

    <?=$form
        ->field($comment, 'postId')
        ->hiddenInput()
        ->label(false)
    ?>

    <?=$form->field($comment, 'body')->textarea() ?>

    <div class="form-group">
        <button type="submit" class="btn btn-success">Save</button>
    </div>

    <?php ActiveForm::end() ?>

    <?php foreach($model->comments as $comment) : ?>
        <p><?=$comment->body ?><span><?=date('Y.m.d. H:i:s', $comment->createdAt)?></span></p>

    <?php endforeach ?>

</div>
