<div class="col-md-8 col-lg-offset-2">
    <h1 class="text-center">
        Add Order
    </h1>
    <form action="/orders/add" method="post" class="order-create">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
        <div class="form-group">
            <label for="customer_name">Customer name</label>
            <input type="text" class="form-control" id="customer_name" name="customer_name">
        </div>
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" id="email" name="email">
        </div>
        <div class="form-group">
            <label for="phone">Phone</label>
            <input type="tel" class="form-control" id="phone" name="phone" >
        </div>
        <div class="form-group">
            <label for="feedback">Example textarea</label>
            <textarea class="form-control" id="feedback" name="feedback"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>
