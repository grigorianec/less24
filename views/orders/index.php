<?php
/** @var \app\models\Order[] $orders */
?>

<div class="orders">
    <h1>
        <?=$title?>
    </h1>
    <div class="list">
        <?php foreach ($orders as $order) :?>
            <div>
                <p class="border">
                    <?=$order->user['username']. '<br>';?>
                    <?=$order->customer_name . '<br>';?>
                    <?=$order->email . '<br>';?>
                    <?=$order->phone . '<br>';?>
                </p>
                <a class="btn btn-primary" href="/orders/view?id=<?=$order->id?>">Show more...</a>
            </div>
        <?php endforeach ;?>
    </div>
</div>