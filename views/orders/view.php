<?php
/** @var \app\models\Order[] $orders */
$comment = new \app\models\Comment();
$comment->postId = $order->id;
use yii\widgets\ActiveForm;
?>
<div class = "text-center">
    <h1>
        <?=$order->customer_name . '<br>';?>
    </h1>
    <p>
        <?=$order->feedback;?><?=$order->id;?>
    </p>
    <a class="btn btn-primary" href="/orders/index">Back</a>
</div>


<?php
$form = ActiveForm::begin([
    'action' => '/comments/create'
])
?>

<?=$form
    ->field($comment, 'postId')
    ->hiddenInput()
    ->label(false)
?>

<?=$form->field($comment, 'body')->textarea() ?>

    <div class="form-group">
        <button type="submit" class="btn btn-success">Save</button>
    </div>

<?php ActiveForm::end() ?>

<?php foreach($order->comments as $comment) : ?>
    <p><?=$comment->body ?><span><?=date('Y.m.d. H:i:s', $comment->createdAt)?></span></p>

<?php endforeach ?>