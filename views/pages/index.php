<div class="pages">
    <h1><?=$title?></h1>
    <ul class="list">
        <?php foreach ($pages as $page) :?>
            <li class="margin">
                <p class="border">
                    <?=$page->title . '<br>';?>
                    <?=$page->alias. '<br>';?>
                </p>
                <a class="btn btn-primary" href="/pages/view?id=<?=$page->id?>">Show more...</a>
                <a class="btn btn-default" href="/pages/update?id=<?=$page->id?>">Update</a>
<!--                <a class="btn btn-danger" href="/pages/delete?id=--><!--">Delete</a>-->
            </li>
        <?php endforeach ;?>
    </ul>
</div>