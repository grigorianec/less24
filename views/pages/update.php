<div class="col-md-8 mr-auto ml-auto">
    <h1>
        Update Product
    </h1>
    <form action="/pages/update?id=<?=$page->id?>" method="post">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="Page[title]" value="<?=$page->title?>">
        </div>
        <div class="form-group">
            <label for="alias">Alias</label>
            <input type="text" class="form-control" id="alias" name="Page[alias]" value="<?=$page->alias?>">
        </div>
        <div class="form-group">
            <label for="content">Description</label>
            <textarea class="form-control" id="content" name="Page[content]"><?=$page->content?></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>

