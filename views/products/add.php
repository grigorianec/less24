<div class="col-md-8 mr-auto ml-auto">
    <h1>
        Add Product
    </h1>
    <form action="/products/add" method="post">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>
        <div class="form-group">
            <label for="alias">Alias</label>
            <input type="text" class="form-control" id="alias" name="alias">
        </div>
        <div class="form-group">
            <label for="descprition">Descrition</label>
            <textarea class="form-control" id="descrpition" name="description"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>
