<div class="products">
    <h1><?=$title?></h1>
    <ul class="list">
        <?php foreach ($products as $key =>  $product) :?>
            <li class="margin">
                <p class="border">
                    <?=$product->title. '<br>';?>
                    <?=$product->alias. '<br>';?>
                </p>
                <a class="btn btn-primary" href="/products/view?id=<?=$product->id?>">Show more...</a>
<!--                <a class="btn btn-default" href="/products/update?id=--><!--">Update</a>-->
<!--                <a class="btn btn-danger" href="/products/delete?id=--><!--">Delete</a>-->
            </li>
        <?php endforeach ;?>
    </ul>
</div>