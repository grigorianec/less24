<div class="col-md-8 mr-auto ml-auto">
    <h1>
        Update Product
    </h1>
    <form action="/products/update?id=<?=$product->id?>" method="post">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="Product[title]" value="<?=$product->title?>">
        </div>
        <div class="form-group">
            <label for="alias">Alias</label>
            <input type="text" class="form-control" id="alias" name="Product[alias]" value="<?=$product->alias?>">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="Product[description]" value="<?=$product->description?>"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>

